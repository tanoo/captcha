package cn.tannn.captcha.api.config;

import cn.tannn.captcha.api.CaptchaCacheService;
import cn.tannn.captcha.api.CaptchaService;
import cn.tannn.captcha.api.impl.CaptchaCacheLocalServiceImpl;
import cn.tannn.captcha.api.impl.CaptchaCacheRedisServiceImpl;
import cn.tannn.captcha.api.impl.CaptchaServiceImpl;
import cn.tannn.captcha.sdk.config.CaptchaSetting;
import cn.tannn.captcha.sdk.dict.CacheMode;
import cn.tannn.captcha.sdk.slide.SlideCaptchaService;
import org.springframework.context.annotation.Bean;

/**
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/12/9 14:06
 */
public class AutoImportCaptchaConfig {

    @Bean
    public CaptchaCacheService captchaCacheService(CaptchaSetting captchaSetting) {
        if(captchaSetting.getCache().equals(CacheMode.REDIS)){
            return new CaptchaCacheRedisServiceImpl(captchaSetting);
        }
        return new CaptchaCacheLocalServiceImpl(captchaSetting);
    }

    @Bean

    public CaptchaService captchaService(CaptchaCacheService captchaCacheService
            , SlideCaptchaService slideCaptchaService) {
        return new CaptchaServiceImpl(captchaCacheService, slideCaptchaService);
    }
}
