package cn.tannn.captcha.api.impl;

import cn.tannn.captcha.api.CaptchaCacheService;
import cn.tannn.captcha.sdk.config.CaptchaSetting;
import cn.tannn.captcha.sdk.module.CaptchaVO;
import cn.tannn.captcha.sdk.util.IpUtil;
import cn.tannn.captcha.sdk.util.StorageUtil;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.time.Duration;
import java.util.Optional;

/**
 * 验证码缓存服务
 *
 * @author tnnn
 * @version V1.0
 * @date 2022-11-21 13:25
 */
public class CaptchaCacheRedisServiceImpl implements CaptchaCacheService {


    private static final Logger LOG = LoggerFactory.getLogger(CaptchaCacheRedisServiceImpl.class);

    /**
     * reids
     */
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    private final CaptchaSetting captchaSetting;

    public CaptchaCacheRedisServiceImpl(CaptchaSetting captchaSetting) {
        this.captchaSetting = captchaSetting;
    }

    @Override
    public CaptchaSetting captchaSetting() {
        return captchaSetting;
    }


    @Override
    public void storageImageCaptcha(CaptchaVO captcha, HttpServletRequest request) {
        String key = IpUtil.getPoxyIpEnhance(request);
        String redisFolderKey = StorageUtil.storageCaptchaRedisFolder(captchaSetting.getPrefix(), key);
        redisTemplate.boundHashOps(redisFolderKey).put(key,
                captcha);
        // 设置过期时间（秒
        redisTemplate.expire(redisFolderKey, Duration.ofSeconds(captcha.getOvertime()));
    }

    @Override
    public Optional<CaptchaVO> loadImageCaptcha(HttpServletRequest request) {
        String key = IpUtil.getPoxyIpEnhance(request);
        return loadImageCaptcha(key);
    }

    @Override
    public Optional<CaptchaVO> loadImageCaptcha(String ip) {
        String redisFolderKey = StorageUtil.storageCaptchaRedisFolder(captchaSetting.getPrefix(), ip);
        try {
            BoundHashOperations<String, Object, CaptchaVO> find = redisTemplate
                    .boundHashOps(redisFolderKey);
            CaptchaVO captchaVO = find.get(ip);
            deleteImageCaptcha(ip);
            return Optional.ofNullable(captchaVO);
        } catch (Exception e) {
            LOG.error("加载验证码信息失败", e);
            return Optional.empty();
        }
    }

    @Override
    public void deleteImageCaptcha(HttpServletRequest request) {
        String key = IpUtil.getPoxyIpEnhance(request);
        deleteImageCaptcha(key);
    }

    @Override
    public void deleteImageCaptcha(String ip) {
        try {
            String redisFolderKey = StorageUtil.storageCaptchaRedisFolder(captchaSetting.getPrefix(), ip);
            redisTemplate.boundHashOps(redisFolderKey).delete(ip);
        } catch (Exception e) {
            LOG.error("删除过期验证码失败", e);
        }
    }
}
