package cn.tannn.captcha.api;

import cn.tannn.captcha.sdk.config.CaptchaSetting;
import cn.tannn.captcha.sdk.module.CaptchaVO;
import jakarta.servlet.http.HttpServletRequest;

import java.util.Optional;

/**
 * 验证码缓存服务
 *
 * @author tnnn
 * @version V1.0
 * @date 2022-11-21 13:21
 */
public interface CaptchaCacheService {

    /**
     * @return 获取 CaptchaSetting
     */
    CaptchaSetting captchaSetting();

    /**
     * 存储图形验证码
     * <p>ip 作为key（保证相对唯一性）</p>
     * @param captcha CaptchaVO
     * @param request 请求信息
     */
    void storageImageCaptcha(CaptchaVO captcha, HttpServletRequest request);

    /**
     * 加载图形验证码
     * <p>ip 作为key（保证相对唯一性）</p>
     * <p>加载的同时会删除缓存</p>
     * @param request 请求信息
     * @return captcha 无数据为空
     */
    Optional<CaptchaVO> loadImageCaptcha(HttpServletRequest request);

    /**
     * 加载图形验证码
     * <p>ip 作为key（保证相对唯一性）</p>
     *  <p>加载的同时会删除缓存</p>
     * @param ip 请求信息
     * @return captcha 无数据为空
     */
    Optional<CaptchaVO> loadImageCaptcha(String ip);


    /**
     * 主动删除图形验证码
     * <p>ip 作为key（保证相对唯一性）</p>
     * @param request 请求信息
     */
    void deleteImageCaptcha(HttpServletRequest request);

    /**
     * 主动删除图形验证码
     * <p>ip 作为key（保证相对唯一性）</p>
     * @param ip 请求信息
     */
    void deleteImageCaptcha(String ip);
}
