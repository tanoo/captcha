package cn.tannn.captcha.api;

import cn.tannn.captcha.sdk.module.CaptchaVO;
import jakarta.servlet.http.HttpServletRequest;

/**
 * 验证码服务
 *
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/12/9 11:43
 */
public interface CaptchaService {
    /**
     * 获取算术验证码
     * @return CaptchaVO
     */
    CaptchaVO math(HttpServletRequest request);

    /**
     * 获取算术验证码(绝对值)
     * @return CaptchaVO
     */
    CaptchaVO absMath(HttpServletRequest request);

    /**
     * 自定义纯数字的验证码（随机4位数字，可重复）
     * @return CaptchaVO
     */
    CaptchaVO number(HttpServletRequest request);

    /**
     * 获取线段干扰验证码
     * @return CaptchaVO
     */
    CaptchaVO line(HttpServletRequest request);

    /**
     * 获取圆圈干扰验证码
     * @return CaptchaVO circle
     */
    CaptchaVO circle(HttpServletRequest request);

    /**
     * 获取扭曲干扰验证码
     * @return CaptchaVO
     */
    CaptchaVO shear(HttpServletRequest request);

    /**
     * 获取拼图验证码
     * @return CaptchaVO
     */
    CaptchaVO slide(HttpServletRequest request);



    /**
     * 验证验证码的正确性
     * <p>  no try 正确 </p>
     * @param answer 用户回答的答案
     * @param request  HttpServletRequest
     */
    void verifyCaptcha(String answer, HttpServletRequest request) throws IllegalAccessException;

    /**
     * 验证验证码的正确性
     * <p>  no try 正确 </p>
     * @param answer 用户回答的答案
     * @param ip  ip
     */
    void verifyCaptcha(String answer, String ip) throws IllegalAccessException;
}
