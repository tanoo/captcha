package cn.tannn.captcha.api.impl;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.captcha.ShearCaptcha;
import cn.hutool.captcha.generator.MathGenerator;
import cn.hutool.captcha.generator.RandomGenerator;
import cn.tannn.captcha.api.CaptchaCacheService;
import cn.tannn.captcha.api.CaptchaService;
import cn.tannn.captcha.sdk.dict.CaptchaType;
import cn.tannn.captcha.sdk.exception.CaptChaException;
import cn.tannn.captcha.sdk.exception.CaptChaExceptionMsg;
import cn.tannn.captcha.sdk.generator.AbsMathGenerator;
import cn.tannn.captcha.sdk.module.CaptchaVO;
import cn.tannn.captcha.sdk.module.SlideCaptcha;
import cn.tannn.captcha.sdk.service.factory.VerifyCaptchaFactory;
import cn.tannn.captcha.sdk.slide.SlideCaptchaService;
import jakarta.servlet.http.HttpServletRequest;

import java.util.Objects;

/**
 * 验证码服务
 *
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/12/9 11:44
 */
public class CaptchaServiceImpl implements CaptchaService {

    private final CaptchaCacheService captchaCacheService;
    /**
     * 滑动验证码
     */
    private final SlideCaptchaService slideCaptchaService;

    public CaptchaServiceImpl(CaptchaCacheService captchaCacheService
            , SlideCaptchaService slideCaptchaService) {
        this.captchaCacheService = captchaCacheService;
        this.slideCaptchaService = slideCaptchaService;
    }

    @Override
    public CaptchaVO math(HttpServletRequest request) {
        ShearCaptcha captcha = CaptchaUtil.createShearCaptcha(160, 45, 4, 1);
        // 自定义验证码内容为四则运算方式
        captcha.setGenerator(new MathGenerator(1));
        // 重新生成code
        captcha.createCode();
        CaptchaVO build = new CaptchaVO(
                captcha.getImageBase64Data()
                , captcha.getCode()
                , captchaCacheService.captchaSetting().getExpireTime()
                , CaptchaType.MATH
        );
        // 存储，验证用
        captchaCacheService.storageImageCaptcha(build, request);
        return build;
    }

    @Override
    public CaptchaVO absMath(HttpServletRequest request) {
        ShearCaptcha captcha = CaptchaUtil.createShearCaptcha(160, 45, 4, 1);
        // 自定义验证码内容为四则运算方式
        captcha.setGenerator(new AbsMathGenerator(1));
        // 重新生成code
        captcha.createCode();
        CaptchaVO build = new CaptchaVO(
                captcha.getImageBase64Data()
                , captcha.getCode()
                , captchaCacheService.captchaSetting().getExpireTime()
                , CaptchaType.MATH
        );
        // 存储，验证用
        captchaCacheService.storageImageCaptcha(build, request);
        return build;
    }

    @Override
    public CaptchaVO number(HttpServletRequest request) {
        RandomGenerator randomGenerator = new RandomGenerator("0123456789", 4);

        LineCaptcha captcha = CaptchaUtil.createLineCaptcha(200, 100);
        captcha.setGenerator(randomGenerator);
        // 重新生成code
        captcha.createCode();
        CaptchaVO build = new CaptchaVO(
                captcha.getImageBase64Data()
                , captcha.getCode()
                , captchaCacheService.captchaSetting().getExpireTime()
                , CaptchaType.NUMBER
        );
        // 存储，验证用
        captchaCacheService.storageImageCaptcha(build, request);
        return build;
    }

    @Override
    public CaptchaVO line(HttpServletRequest request) {
        LineCaptcha captcha = CaptchaUtil.createLineCaptcha(160, 45, 4, 150);
        CaptchaVO build = new CaptchaVO(
                captcha.getImageBase64Data()
                , captcha.getCode()
                , captchaCacheService.captchaSetting().getExpireTime()
                , CaptchaType.LINE
        );
        // 存储，验证用
        captchaCacheService.storageImageCaptcha(build, request);
        return build;
    }

    @Override
    public CaptchaVO circle(HttpServletRequest request) {
        CircleCaptcha captcha = CaptchaUtil.createCircleCaptcha(160, 45, 4, 20);
        CaptchaVO build = new CaptchaVO(
                captcha.getImageBase64Data()
                , captcha.getCode()
                , captchaCacheService.captchaSetting().getExpireTime()
                , CaptchaType.CIRCLE
        );
        // 存储，验证用
        captchaCacheService.storageImageCaptcha(build, request);
        return build;
    }

    @Override
    public CaptchaVO shear(HttpServletRequest request) {
        ShearCaptcha captcha = CaptchaUtil.createShearCaptcha(160, 45, 4, 4);

        CaptchaVO build = new CaptchaVO(
                captcha.getImageBase64Data()
                , captcha.getCode()
                , captchaCacheService.captchaSetting().getExpireTime()
                , CaptchaType.SHEAR
        );
        // 存储，验证用
        captchaCacheService.storageImageCaptcha(build, request);
        return build;
    }

    @Override
    public CaptchaVO slide(HttpServletRequest request) {
        SlideCaptcha captcha = slideCaptchaService.getCaptcha(request);
        // 缓存
        CaptchaVO build = new CaptchaVO();
        build.setOvertime(captchaCacheService.captchaSetting().getExpireTime());
        // getBlockX -  answer(SlideCaptcha.value) < 允许偏差
        build.setCaptcha(String.valueOf(Objects.isNull(captcha.getBlockX()) ? "0" : captcha.getBlockX()));
        build.setCaptchaType(CaptchaType.SLIDE);

        // 存储，验证用
        captchaCacheService.storageImageCaptcha(build, request);
        // 这个不能返回给前端
        captcha.setBlockX(null);
        // 这个没必要存
        build.setSlide(captcha);
        return build;
    }


    @Override
    public void verifyCaptcha(String answer, HttpServletRequest request) throws IllegalAccessException {
        CaptchaVO redisCaptcha = captchaCacheService.loadImageCaptcha(request)
                .orElseThrow(() -> new CaptChaException(CaptChaExceptionMsg.CAPTCHA));
        Boolean aBoolean = verifyCaptcha(answer, redisCaptcha);
        if (!aBoolean) {
            throw new CaptChaException(CaptChaExceptionMsg.CAPTCHA);
        }
    }

    @Override
    public void verifyCaptcha(String answer, String ip) throws IllegalAccessException {
        CaptchaVO redisCaptcha = captchaCacheService.loadImageCaptcha(ip)
                .orElseThrow(() -> new CaptChaException(CaptChaExceptionMsg.CAPTCHA));
        Boolean aBoolean = verifyCaptcha(answer, redisCaptcha);
        if (!aBoolean) {
            throw new CaptChaException(CaptChaExceptionMsg.CAPTCHA);
        }
    }

    /**
     * 验证验证码
     *
     * @param answer       回答
     * @param redisCaptcha 问题
     * @return ture 对了
     * @throws IllegalAccessException IllegalAccessException
     */
    private static Boolean verifyCaptcha(String answer, CaptchaVO redisCaptcha) throws IllegalAccessException {
        String question = redisCaptcha.getCaptcha();
        VerifyCaptchaFactory captchaFactory = new VerifyCaptchaFactory(redisCaptcha.getCaptchaType());
        return captchaFactory.verifyCaptcha(question, answer);
    }
}
