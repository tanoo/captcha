package cn.tannn.captcha.server.interfaces;

import cn.tannn.captcha.api.CaptchaService;
import cn.tannn.jdevelops.result.response.ResultVO;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 验证码的验证接口
 *
 * @author tnnn
 * @version V1.0
 * @date 2022-09-01 10:07
 */
@RestController
@RequestMapping("/verify/captcha")
public class CaptChaVerifyCtr {

    private final CaptchaService captchaService;

    public CaptChaVerifyCtr(CaptchaService captchaService) {
        this.captchaService = captchaService;
    }

    /**
     * 验证验证码的正确性
     *
     * @param answer 用户回答的答案
     * @return ture or false
     */
    @GetMapping("/{answer}")
    public ResultVO<Boolean> imageMathCaptcha(@PathVariable("answer") String answer,
                                              HttpServletRequest request) throws IllegalAccessException {
        captchaService.verifyCaptcha(answer, request);
        return ResultVO.successMessage("验证码正确成功");
    }


    /**
     * 验证验证码的正确性(指定IP，这个接口尽量不外暴露，做微服务的话这个接口抽离成fegin)
     *
     * @param answer 用户回答的答案
     * @return ture or false
     */
    @GetMapping("/{answer}/{ip}")
    public ResultVO<Boolean> imageMathCaptcha(@PathVariable("answer") String answer,
                                              @PathVariable("ip") String ip) throws IllegalAccessException {
        captchaService.verifyCaptcha(answer, ip);
        return ResultVO.successMessage("验证码正确成功");
    }



}
