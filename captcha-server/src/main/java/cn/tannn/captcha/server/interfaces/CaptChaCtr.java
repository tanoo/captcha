package cn.tannn.captcha.server.interfaces;

import cn.tannn.captcha.api.CaptchaService;
import cn.tannn.captcha.sdk.module.CaptchaVO;
import cn.tannn.jdevelops.result.response.ResultVO;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * 验证码的生成接口
 *
 * @author tnnn
 * @version V1.0
 * @date 2022-09-01 10:07
 */
@RestController
@RequestMapping("/captcha")
public class CaptChaCtr {

    private final CaptchaService captchaService;

    public CaptChaCtr(CaptchaService captchaService) {
        this.captchaService = captchaService;
    }

    /**
     * 算术图形验证码
     *
     * @param abs 1:结果非负数，0和其他正常的算数
     * @return CaptchaVO of ResultVO
     */
    @GetMapping("/math")
    public ResultVO<CaptchaVO> imageMathCaptcha(@RequestParam(name = "abs", required = false) Integer abs, HttpServletRequest request) {
        if (abs != null && abs == 1) {
            return ResultVO.success(captchaService.absMath(request));
        } else {
            return ResultVO.success(captchaService.math(request));
        }
    }

    /**
     * 纯数字图形验证码
     *
     * @return CaptchaVO of ResultVO
     */
    @GetMapping("/number")
    public ResultVO<CaptchaVO> imageNumberCaptcha(HttpServletRequest request) {
        return ResultVO.success(captchaService.number(request));
    }


    /**
     * 线段干扰图形验证码
     *
     * @return CaptchaVO of ResultVO
     */
    @GetMapping("/line")
    public ResultVO<CaptchaVO> imageLineCaptcha(HttpServletRequest request) {
        return ResultVO.success(captchaService.line(request));
    }


    /**
     * 圆圈干扰图形验证码
     *
     * @return CaptchaVO of ResultVO
     */
    @GetMapping("/circle")
    public ResultVO<CaptchaVO> imageCircleCaptcha(HttpServletRequest request) {
        return ResultVO.success(captchaService.circle(request));
    }


    /**
     * 扭曲干扰图形验证码
     *
     * @return CaptchaVO of ResultVO
     */
    @GetMapping("/shear")
    public ResultVO<CaptchaVO> imageShearCaptcha(HttpServletRequest request) {
        return ResultVO.success(captchaService.shear(request));
    }


    /**
     * 滑动验证码
     *
     * @return CaptchaVO of ResultVO
     */
    @GetMapping("/slide")
    public ResultVO<CaptchaVO> imageSlideCaptcha(HttpServletRequest request) {
        return ResultVO.success(captchaService.slide(request));
    }


}
