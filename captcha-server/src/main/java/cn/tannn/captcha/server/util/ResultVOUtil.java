package cn.tannn.captcha.server.util;

import cn.tannn.captcha.sdk.exception.CaptChaExceptionMsg;
import cn.tannn.jdevelops.result.response.ResultVO;

/**
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/12/6 16:20
 */
public class ResultVOUtil {
    public static ResultVO<Boolean> resultMsg(boolean result, String message) {
        if (result) {

        }
        return ResultVO.fail(CaptChaExceptionMsg.CAPTCHA.getMessage(), result);
    }
}
