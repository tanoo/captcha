package cn.tannn.captcha.sdk.service.impl;

import cn.hutool.captcha.generator.MathGenerator;
import cn.hutool.captcha.generator.RandomGenerator;
import cn.tannn.captcha.sdk.exception.CaptChaException;
import cn.tannn.captcha.sdk.exception.CaptChaExceptionMsg;
import cn.tannn.captcha.sdk.service.VerifyCaptcha;
import cn.tannn.captcha.sdk.util.StringUtil;

/**
 * 滑动验证码比对验证
 * @author tnnn
 * @version V1.0
 * @date 2023-02-26 18:17
 */
public class NumberVerifyCaptcha implements VerifyCaptcha {


    @Override
    public Boolean verifyCaptcha(String question, String answer) {
        if (StringUtil.isNotBlank(question) && StringUtil.isNotBlank(answer)) {
            return new RandomGenerator("0123456789", 4).verify(question, answer);
        }
        throw new CaptChaException("纯数字验证码参数异常");
    }
}
