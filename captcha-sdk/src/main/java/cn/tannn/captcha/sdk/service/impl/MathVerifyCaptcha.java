package cn.tannn.captcha.sdk.service.impl;

import cn.hutool.captcha.generator.MathGenerator;
import cn.tannn.captcha.sdk.exception.CaptChaException;
import cn.tannn.captcha.sdk.service.VerifyCaptcha;
import cn.tannn.captcha.sdk.util.StringUtil;

/**
 * 算术验证
 *
 * @author tnnn
 * @version V1.0
 * @date 2023-02-26 18:17
 */
public class MathVerifyCaptcha implements VerifyCaptcha {

    @Override
    public Boolean verifyCaptcha(String question, String answer) {
        if (StringUtil.isNotBlank(question) && StringUtil.isNotBlank(answer)) {
            return new MathGenerator().verify(question, answer);
        }
        throw new CaptChaException("算术验证码参数异常");
    }
}
