package cn.tannn.captcha.sdk.exception;

import java.io.Serial;

/**
 * 验证码异常
 *
 * @author tnnn
 * @version V1.0
 * @date 2022-11-21 13:51
 */
public class CaptChaException extends RuntimeException{

    @Serial
    private static final long serialVersionUID = 4129812562603997310L;

    private int code;
    private String msg;

    public CaptChaException() {
        super();
    }

    public CaptChaException(String message) {
        super(message);
        this.msg = message;
        this.code = 500;
    }

    public CaptChaException(Integer code, String message) {
        super(message);
        this.code = code;
        this.msg = message;
    }

    public CaptChaException(CaptChaExceptionMsg message) {
        super(message.getMessage());
        this.code = message.getCode();
        this.msg = message.getMessage();
    }

    public CaptChaException(String message, Throwable cause) {
        super(message, cause);
        this.msg = message;
        this.code = 500;
    }

    public CaptChaException(String message, Throwable cause, int code) {
        super(message, cause);
        this.msg = message;
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
