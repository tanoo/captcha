package cn.tannn.captcha.sdk.module;


import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 滑动验证码
 * @author tnnn
 */
@Schema(description = "滑动验证码")
public class SlideCaptcha {

    /**
     * 随机字符串
     **/
    @Schema(description = "随机字符串")
    private String nonceStr;
    /**
     * 验证值
     **/
    @Schema(description = "验证值")
    private String value;
    /**
     * 生成的画布的base64
     **/
    @Schema(description = "生成的画布的base64")
    private String canvasSrc;
    /**
     * 画布宽度
     **/
    @Schema(description = "画布宽度")
    private Integer canvasWidth;
    /**
     * 画布高度
     **/
    @Schema(description = "画布高度")
    private Integer canvasHeight;
    /**
     * 生成的阻塞块的base64
     **/
    @Schema(description = "生成的阻塞块的base64")
    private String blockSrc;
    /**
     * 阻塞块宽度
     **/
    @Schema(description = "阻塞块宽度")
    private Integer blockWidth;
    /**
     * 阻塞块高度
     **/
    @Schema(description = "阻塞块高度")
    private Integer blockHeight;
    /**
     * 阻塞块凸凹半径
     **/
    @Schema(description = "阻塞块凸凹半径")
    private Integer blockRadius;
    /**
     * 阻塞块的横轴坐标
     **/
    @Schema(description = "阻塞块的横轴坐标")
    private Integer blockX;
    /**
     * 阻塞块的纵轴坐标
     **/
    @Schema(description = "阻塞块的纵轴坐标")
    private Integer blockY;

    public SlideCaptcha() {
    }

    public SlideCaptcha(String nonceStr, String value, String canvasSrc, Integer canvasWidth, Integer canvasHeight, String blockSrc, Integer blockWidth, Integer blockHeight, Integer blockRadius, Integer blockX, Integer blockY) {
        this.nonceStr = nonceStr;
        this.value = value;
        this.canvasSrc = canvasSrc;
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        this.blockSrc = blockSrc;
        this.blockWidth = blockWidth;
        this.blockHeight = blockHeight;
        this.blockRadius = blockRadius;
        this.blockX = blockX;
        this.blockY = blockY;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCanvasSrc() {
        return canvasSrc;
    }

    public void setCanvasSrc(String canvasSrc) {
        this.canvasSrc = canvasSrc;
    }

    public Integer getCanvasWidth() {
        return canvasWidth;
    }

    public void setCanvasWidth(Integer canvasWidth) {
        this.canvasWidth = canvasWidth;
    }

    public Integer getCanvasHeight() {
        return canvasHeight;
    }

    public void setCanvasHeight(Integer canvasHeight) {
        this.canvasHeight = canvasHeight;
    }

    public String getBlockSrc() {
        return blockSrc;
    }

    public void setBlockSrc(String blockSrc) {
        this.blockSrc = blockSrc;
    }

    public Integer getBlockWidth() {
        return blockWidth;
    }

    public void setBlockWidth(Integer blockWidth) {
        this.blockWidth = blockWidth;
    }

    public Integer getBlockHeight() {
        return blockHeight;
    }

    public void setBlockHeight(Integer blockHeight) {
        this.blockHeight = blockHeight;
    }

    public Integer getBlockRadius() {
        return blockRadius;
    }

    public void setBlockRadius(Integer blockRadius) {
        this.blockRadius = blockRadius;
    }

    public Integer getBlockX() {
        return blockX;
    }

    public void setBlockX(Integer blockX) {
        this.blockX = blockX;
    }

    public Integer getBlockY() {
        return blockY;
    }

    public void setBlockY(Integer blockY) {
        this.blockY = blockY;
    }

    @Override
    public String toString() {
        return "SlideCaptcha{" +
                "nonceStr='" + nonceStr + '\'' +
                ", value='" + value + '\'' +
                ", canvasSrc='" + canvasSrc + '\'' +
                ", canvasWidth=" + canvasWidth +
                ", canvasHeight=" + canvasHeight +
                ", blockSrc='" + blockSrc + '\'' +
                ", blockWidth=" + blockWidth +
                ", blockHeight=" + blockHeight +
                ", blockRadius=" + blockRadius +
                ", blockX=" + blockX +
                ", blockY=" + blockY +
                '}';
    }
}
