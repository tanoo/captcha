package cn.tannn.captcha.sdk.module;

import cn.tannn.captcha.sdk.dict.CaptchaType;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * 验证码vo
 *
 * @author tn
 * @date 2021-09-13 15:59
 */
@Schema(description = "验证码")
public class CaptchaVO  implements Serializable {
    /**
     *  普通验证码base64(画布)
     */
    @Schema(description = "普通验证码base64")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String base64;

    /**
     * 滑动验证特有字段
     */
    @Schema(description = "滑动验证内容详细数据")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private SlideCaptcha slide;

    /**
     * 验证码数据
     */
    @Schema(description = "验证码数据")
    private String captcha;

    /**
     *  过期时间（秒）
     */
    @Schema(description = "过期时间（秒）")
    private Long overtime;

    /**
     *  验证码类型
     */
    @Schema(description = "验证码类型（秒）")
    private CaptchaType captchaType;

    public CaptchaVO() {
    }


    public CaptchaVO(String base64, String captcha, Long overtime, CaptchaType captchaType) {
        this.base64 = base64;
        this.captcha = captcha;
        this.overtime = overtime;
        this.captchaType = captchaType;
    }


    public CaptchaVO(String base64, SlideCaptcha slide, String captcha, Long overtime, CaptchaType captchaType) {
        this.base64 = base64;
        this.slide = slide;
        this.captcha = captcha;
        this.overtime = overtime;
        this.captchaType = captchaType;
    }


    @Override
    public String toString() {
        return "CaptchaVO{" +
                "base64='" + base64 + '\'' +
                ", slide=" + slide +
                ", captcha='" + captcha + '\'' +
                ", overtime=" + overtime +
                ", captchaType=" + captchaType +
                '}';
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public SlideCaptcha getSlide() {
        return slide;
    }

    public void setSlide(SlideCaptcha slide) {
        this.slide = slide;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public Long getOvertime() {
        return overtime;
    }

    public void setOvertime(Long overtime) {
        this.overtime = overtime;
    }

    public CaptchaType getCaptchaType() {
        return captchaType;
    }

    public void setCaptchaType(CaptchaType captchaType) {
        this.captchaType = captchaType;
    }
}
