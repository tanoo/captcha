package cn.tannn.captcha.sdk.service.impl;

import cn.tannn.captcha.sdk.exception.CaptChaException;
import cn.tannn.captcha.sdk.service.VerifyCaptcha;
import cn.tannn.captcha.sdk.util.StringUtil;

/**
 * 常规比对验证
 * 包含： 线段，圆圈，扭曲
 * @author tnnn
 * @version V1.0
 * @date 2023-02-26 18:17
 */
public class ComparisonVerifyCaptcha implements VerifyCaptcha {

    @Override
    public Boolean verifyCaptcha(String question, String answer) {
        if (StringUtil.isNotBlank(question) && StringUtil.isNotBlank(answer)) {
            return question.equals(answer);
        }
        throw new CaptChaException("验证码参数异常");
    }
}
