package cn.tannn.captcha.sdk.util;

/**
 * 存储相关的工具类
 * @author tnnn
 * @version V1.0
 * @date 2022-11-21 13:38
 */
public class StorageUtil {

    /**
     * 验证码缓存key
     */
    public static final String CAPTCHA_CACHE_KEY = "CAPTCHA_CACHE";


    /**
     * 验证码的存储key
     *
     * @param prefix 前缀（一般是项目名
     * @param key    key
     * @return folderName:key
     */
    public static String storageCaptchaRedisFolder(String prefix, String key) {
        return prefix + ":" + CAPTCHA_CACHE_KEY + ":" + key;
    }


}
