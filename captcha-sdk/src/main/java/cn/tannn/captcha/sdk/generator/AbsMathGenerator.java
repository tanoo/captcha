package cn.tannn.captcha.sdk.generator;

import cn.hutool.captcha.generator.CodeGenerator;
import cn.hutool.core.math.Calculator;
import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 数字计算验证码生成器(绝对值)
 *
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2025/1/6 12:13
 */
public class AbsMathGenerator implements CodeGenerator {
    private static final long serialVersionUID = -5514819971774091076L;
    private static final Logger LOG = LoggerFactory.getLogger(AbsMathGenerator.class);

    private static final String operators = "+-*";

    /** 参与计算数字最大长度 */
    private final int numberLength;

    /**
     * 构造
     */
    public AbsMathGenerator() {
        this(2);
    }

    /**
     * 构造
     *
     * @param numberLength 参与计算最大数字位数
     */
    public AbsMathGenerator(int numberLength) {
        this.numberLength = numberLength;
    }

    @Override
    public String generate() {
        final int limit = getLimit();
        String number1 = Integer.toString(RandomUtil.randomInt(limit));
        String number2 = Integer.toString(RandomUtil.randomInt(limit));

        // 确保如果运算符是 "-"，第一个数字大于等于第二个数字
        char operator = RandomUtil.randomChar(operators);
        if (operator == '-' && Integer.parseInt(number1) < Integer.parseInt(number2)) {
            // 如果是减法且第一个数字小于第二个数字，则交换两个数字
            String temp = number1;
            number1 = number2;
            number2 = temp;
            LOG.info("math captcha abs dispose");
        }

        number1 = StrUtil.padAfter(number1, this.numberLength, CharUtil.SPACE);
        number2 = StrUtil.padAfter(number2, this.numberLength, CharUtil.SPACE);

        return StrUtil.builder()//
                .append(number1)// 第一个数字
                .append(operator)// 运算符
                .append(number2)// 第二个数字
                .append('=')// 等号
                .toString();
    }

    @Override
    public boolean verify(String code, String userInputCode) {
        int result;
        try {
            result = Integer.parseInt(userInputCode);
        } catch (NumberFormatException e) {
            // 用户输入非数字
            return false;
        }

        final int calculateResult = (int) Calculator.conversion(code);
        return result == calculateResult;
    }

    /**
     * 获取验证码长度
     *
     * @return 验证码长度
     */
    public int getLength() {
        return this.numberLength * 2 + 2;
    }

    /**
     * 根据长度获取参与计算数字最大值
     *
     * @return 最大值
     */
    private int getLimit() {
        return Integer.parseInt("1" + StrUtil.repeat('0', this.numberLength));
    }
}
