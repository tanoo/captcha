package cn.tannn.captcha.sdk.dict;

/**
 * 缓存模式
 * @author tan
 * @date 2023-02-26 18:05:25
 */
public enum CacheMode {

    /**
     * 本地缓存
     */
    LOCAL,

    /**
     * redis缓存
     */
    REDIS

}
